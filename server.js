var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/peakmieapiDB');
var PMModel = require('./api/models/peakmieapiModel')
var express=require('express');
var morgan = require('morgan');
var http=require('http');
var bodyParser= require('body-parser');
var methodOverride = require('method-override');
var mongo = require('mongojs');
var session = require('express-session');
var api = require('./api/controllers/peakmieapiController');
var express = require('express'),
  app = express(),
  //app = module.exports = express(),
  port = process.env.PORT || 8001,
  mongoose = require('mongoose'),
  bodyParser = require('body-parser');
app.use(express.static('./assets/user_profile/'));
app.use(morgan('dev'));                     // log every request to the console
app.use(bodyParser.urlencoded({ extended: false,limit: '5mb' }));    // parse application/x-www-form-urlencoded
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//app.use(methodOverride());    
app.post('/uploadUserPic',api.upload); 
//app.get('/signin/:userid',api.signin);
var routes = require('./api/routes/peakmieapiRoutes'); //importing route
routes(app); //register the route
app.listen(port);
console.log('peakmie RESTful API server started on: ' + port);