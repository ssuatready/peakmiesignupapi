'use strict';
//import fetch from 'node-fetch';
var fetchjson = require('node-fetch-json');
var fetch = require('node-fetch');
const Request = require("request");
var mongoose = require('mongoose'),
  Task = mongoose.model('signupapis');
// var http = require('http');
// var formidable = require('formidable');
// var fs = require('fs');
var express=require('express');
var app=express.Router();
var multer = require('multer');
var async = require("async");
var moment = require("moment");
var request = require('request');
var storage2 = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null,__dirname+'/assets/user_profile');
  },
  filename: function (req, file, callback) {
    callback(null,file.originalname);
  }
});

var upload = multer({ storage: storage2}).single('image');
exports.list_all_tasks = function(req, res) {
  //res.sendFile("me.jpeg",{ root: __dirname });
  Task.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.create_a_task = function(req, res) {
  //console.log(req.body);
  var new_task = new Task(req.body);
  new_task.save(function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
  upload(req, res, function(err){
    if (err) {
        return res.status(500).json({
            status: "Error",
            msg: err+"Profile picture not uploaded"
        });
    }
    else{
        return res.status(200).json({
            status: "OK",
            msg: "Profile picture uploaded"
        });
    }
  });
};
exports.upload_user_pic = function(req, res) {

  upload(req, res, function(err){
    if (err) {
        return res.status(500).json({
            status: "Error",
            msg: err+"Profile picture not uploaded"
        });
    }
    else{
        return res.status(200).json({
            status: "OK",
            msg: "Profile picture uploaded"
        });
    }
});
};

exports.read_a_task = function(req, res) {
  var query = { username: req.params.userid };
  Task.findOne(query, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
    });
};
exports.login = function(req, res){
//   var https = require('https')
//   var host = 'github.com';
//   //const Request = require("request");
//   try{
//   var request = https.get(host, function(response){
//   console.log(response.statusCode);
//   // console.log(JSON.parse(body));
//   // response.json.parse((body));
//   }).on("error", function(error) {
//     console.log(error.message);
// });
// }catch(e) {
//   console.log(e);
// }


// const options = {
//   method: 'GET  ',
//   headers: {
//      'Content-Type': 'application/json',
//      'Accept': 'application/json'
//      }
//   };
//   fetch('https://jsonplaceholder.typicode.com/todos/1', options)
//    .then(response => response.json())
//    .catch(console.log);

  Task.find({username:req.params.userid},{username:false,gender:false,_id:false,status:false,confirm_password:false,name:false}, function(err, task) {
    if (err)
      res.send(err);
      res.json(task);
      //fetch('http://localhost:8000/profilepic/pratik-asthana')
       // .then((response) => {
          
          // const value = {
          //   result,
          //   task
          // };
          // res.json(value.result);
          res.sendFile(req.params.userid+".png",{ root: __dirname+'/assets/user_profile' });
      //  });
  });
    
};

exports.send_profile_pic = function(req, res) {
  var query = { username: req.params.userid };
  res.set({'Content-Type': 'image/png'});
  res.sendFile(req.params.userid+".png",{ root: __dirname+'/assets/user_profile' });
};


exports.update_a_task = function(req, res) {
  Task.findOneAndUpdate({username: req.params.userid}, req.body, {new: true}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};



exports.delete_a_task = function(req, res) {
  Task.remove({
    username: req.params.userid
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'Task successfully deleted' });
  });
};



// var express=require('express');
// var app=express.Router();
// var multer = require('multer');
// var async = require("async");
// var moment = require("moment");
// var request = require('request');
// var storage2 = multer.diskStorage({
//   destination: function (req, file, callback) {
//     callback(null,__dirname+'/assets/user_profile');
//   },
//   filename: function (req, file, callback) {
//     callback(null,file.originalname);
//   }
// });
//var upload = multer({ storage: storage2}).single('image');

// app.post('/uploaduserprofile', function (req, res) {
//     upload(req, res, function(err){
//         if (err) {
//             return res.status(500).json({
//                 status: "Error",
//                 msg: err+"Profile picture not uploaded"
//             });
//         }
//         else{
//             return res.status(200).json({
//                 status: "OK",
//                 msg: "Profile picture uploaded"
//             });
//         }
//     });
// });

// exports.upload=function(req,res){
// }
exports.upload=app;