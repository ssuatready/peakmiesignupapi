'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var signupSchema = new Schema({
  name: {
    type: String,
    required: 'Kindly enter your name'
  },
  username: {
    type: String,
    required: 'Kindly enter your username'
  },
  email:{
    type: String,
    required: 'Kindly enter your email address'
  },
  password: {
    type: String,
    required: 'Kindly enter your password'
  },
  confirm_password: {
    type: String,
    required: 'Kindly re-enter your password'
  },
  gender: {
    type: String,
    required: 'Kindly enter your gender'
  },
  status: {
    type: [{
      type: String,
      enum: ['pending', 'ongoing', 'completed']
    }],
    default: ['pending']
  }
});

module.exports = mongoose.model('signupapis', signupSchema);