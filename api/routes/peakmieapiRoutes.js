'use strict';
module.exports = function(app) {
  var peakmie = require('../controllers/peakmieapiController');


  // pickme Routes
  app.route('/signupapi')
    .get(peakmie.list_all_tasks)
    .post(peakmie.create_a_task);
  app.route('/signupapi/:userid')
    .get(peakmie.read_a_task)
    //.get(peakmie.send_profile_pic)
    .put(peakmie.update_a_task)
    .delete(peakmie.delete_a_task);
  app.route('/profilepic/:userid')
    .get(peakmie.send_profile_pic);
  app.route('/uploadUserPic')
    .post(peakmie.upload_user_pic);
  app.route('/signin/:userid')
    .get(peakmie.login);
  
};
